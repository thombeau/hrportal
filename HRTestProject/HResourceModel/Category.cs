﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HResourceModel
{
    public class Category
    {
        public int CategoryNo { get; set; }
        public string CategoryType { get; set; }
    }
}
